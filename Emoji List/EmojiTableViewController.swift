//
//  EmojiTableViewController.swift
//  Emoji List
//
//  Created by umar on 10/13/17.
//  Copyright © 2017 iOS. All rights reserved.
//

import UIKit

class EmojiTableViewController: UITableViewController {
    @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {
        // make table view in editing mode
        let tableViewEditingMode = tableView.isEditing
        tableView.setEditing(tableViewEditingMode, animated: true)
    }
    
    var emojis: [Emoji] = [
        Emoji(symbol: "😀", name: "Grinning Face", description: "A typical smilley face.", usage: "happines"),
        Emoji(symbol: "😕", name: "Confused Face", description: "A confused, puzzled face.", usage: "unsure what to think; displeasure"),
        Emoji(symbol: "😍", name: "Heart Eyes", description: "A miley faces with heart of eyes.", usage: "love of something, attractive"),
        Emoji(symbol: "👮", name: "Police Officer", description: "A police officer with blue cap and gold badge.", usage: "person of authority"),
        Emoji(symbol: "🐢", name: "Turtle", description: "A cute turtle.", usage: "something slow"),
        Emoji(symbol: "🐘", name: "Elephant", description: "A grey elephant.", usage: "good memory"),
        Emoji(symbol: "🍝", name: "Spaghetti", description: "A plate of spaghetti.", usage: "spaghetti"),
        Emoji(symbol: "🎲", name: "Die", description: "A single die.", usage: "taking risk; chance; game"),
        Emoji(symbol: "⛺️", name: "Tent", description: "A small tent.", usage: "camping"),
        Emoji(symbol: "📚", name: "Stack of Books",description: "Three colored books stacked on each other.",       usage: "homework, studying"),
        Emoji(symbol: "💔", name: "Broken Heart",description: "A red, broken heart.", usage: "extreme            sadness"),
        Emoji(symbol: "💤", name: "Snore",description:"Three blue \'z\'s.", usage: "tired, sleepiness"),
        Emoji(symbol: "🏁", name: "Checkered Flag",description: "A black-and-white checkered flag.", usage:          "completion"),
        Emoji(symbol: "😈", name: "Purple Devil", description: "A purple colored devil face.", usage: "funny evilness")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditEmoji" {
            let indexPath = tableView.indexPathForSelectedRow!
            let emoji = emojis[indexPath.row]
            let addEditEmojiTableViewController = segue.destination as! AddEmojiTableViewController
            addEditEmojiTableViewController.emoji = emoji
        }
    }
    
    @IBAction func unwindEmojiTableView(with segue: UIStoryboardSegue){
        tableView.setEditing(false, animated: false)
        
        guard segue.identifier == "saveUnwind" else { return }
        print("saved")
        
        let viewSourceController = segue.source as! AddEmojiTableViewController
        
        if let emoji = viewSourceController.emoji {
            //edit existing emoji
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                print("can edit table view")
                emojis[selectedIndexPath.row] = emoji
                print(emoji)
                tableView.reloadRows(at: [selectedIndexPath], with: .none) // reload table view that was display earlier
                print("------")
            }else{
                print("can add some table view")
                //inserting new emoji
                let newIndexPath = IndexPath(row: emojis.count, section: 0)
                emojis.append(emoji)
                tableView.insertRows(at: [newIndexPath], with: .automatic) // insert a new cell on table view
                print("------ ")
            }
        }
    }
    
    @IBAction  func refreshControlActivated(_ sender: UIRefreshControl) {
        
        
        tableView.reloadData()
        sender.endRefreshing()
    }
    
}



extension EmojiTableViewController{
    
    // data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return emojis.count
        }else{
            return 0
        }
    }
    
    // set the cell for displaying data
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // managing the cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmojiCell", for: indexPath) as! EmojiTableViewCell
        let emoji = emojis[indexPath.row]
        
        cell.update(with: emoji)
        
        //        cell.textLabel?.text = "\(emoji.symbol) - \(emoji.name)"
        //        cell.detailTextLabel?.text = emoji.description
        
        cell.showsReorderControl = true
        
        return cell
    }
    //------------------------------
    // table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let emoji = emojis[indexPath.row]
        print ("\(emoji.symbol) , \(indexPath)")
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            emojis.remove(at: indexPath.row) // delete content drom array emojis
            tableView.deleteRows(at: [indexPath], with: .automatic)  // delete row from table view
        }
    }
    
}












// MARK -> comment template from xcode




/*
 override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
 
 // Configure the cell...
 
 return cell
 }
 */

/*
 // Override to support conditional editing of the table view.
 override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
 // Return false if you do not want the specified item to be editable.
 return true
 }
 */

/*
 // Override to support editing the table view.
 override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
 if editingStyle == .delete {
 // Delete the row from the data source
 tableView.deleteRows(at: [indexPath], with: .fade)
 } else if editingStyle == .insert {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
 
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
 // Return false if you do not want the item to be re-orderable.
 return true
 }
 */

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */
